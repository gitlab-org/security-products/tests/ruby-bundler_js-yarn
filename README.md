# ruby-bundler_js-yarn

Test project with:

* **Languages:** Ruby, Javascript
* **Package Managers:** Bundler, Yarn

## Usage

You can create new Branches and Merge Requests on this project for you own testing purpose but before updating existing branches please check the keyword suffixes:

* `FREEZE`: you must NOT update nor delete this branch without asking a ~"Security Products" member.

If you want to test a specific MR behavior that depends on the state of the target branch then you'd better create a specific target branch instead of master.

### Testing branches

* sast_integration_tests-FREEZE
